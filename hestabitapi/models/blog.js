let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let AutoIncrement = require('mongoose-sequence')(mongoose);

let blogSchema = new Schema({
    id: { type: Number },
    title: { type: String, index: 'text' },
    image: { type: String, default: null },
    description: { type: String, default: null },
    category: { type: String, default: null },
    userId: { type: Schema.Types.ObjectId, default: null },
    created: Date,
    updated: Date,
});

// set up a mongoose model and pass it using module.exports
blogSchema.plugin(AutoIncrement, { inc_field: 'id', id: "blogId" });
module.exports = mongoose.model('blog', blogSchema);