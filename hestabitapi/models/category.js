let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let AutoIncrement = require('mongoose-sequence')(mongoose);

let categorySchema = new Schema({
    id: { type: Number },
    name: { type: String },
    lName: { type: String, lowercase: true },
    created: Date,
    updated: Date,
});

// set up a mongoose model and pass it using module.exports
categorySchema.plugin(AutoIncrement, { inc_field: 'id', id: "categoryId" });
module.exports = mongoose.model('category', categorySchema);