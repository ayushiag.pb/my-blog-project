let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let AutoIncrement = require('mongoose-sequence')(mongoose);

let userSchema = new Schema({
    id: { type: Number },
    email: { type: String },
    password: { type: String },
    created: Date,
    updated: Date,
});

// set up a mongoose model and pass it using module.exports
userSchema.plugin(AutoIncrement, { inc_field: 'id', id: "userId" });
module.exports = mongoose.model('user', userSchema);