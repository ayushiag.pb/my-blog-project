'use strict';

const express = require('express')
const app = express()
const bodyParser = require('body-parser');
const config = require('config');
const port = config.port || 3000; // used to create, sign, and verify tokens
var cors = require('cors');


app.use(cors());
// =======================
// database =========
// =======================
const mongoose = require('mongoose');
// =======================
// configuration =========
// =======================
mongoose.connect(config.database, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
}); // connect to database

mongoose.set('useFindAndModify', false);
mongoose.set('debug', true);

const database = mongoose.connection;
database.on('error', console.error.bind(console, 'connection error:'));
database.once('open', () => {
  // we're connected!
  console.log("we're connected!");
});

// =======================
// parse body =========
// =======================
// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var routes = require('./route');

// ...

app.use('/', routes);


// =======================
// serve static files =========
// =======================
app.use(express.static('public'))

// =======================
// server start on port =========
// =======================
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})