var express = require('express')
var router = express.Router()
var controller = require('./controller');


/**********************************Unsecure Routes *****************************************/


//user
router.post('/login', controller.user.login)

router.post('/register', controller.user.register)

//blog
router.get('/blog', controller.blog.list)

router.get('/blog/:blogId', controller.blog.details)


router.use(controller.authenticator)

/**********************************Secure Routes *****************************************/

router.post('/blog', controller.blog.create)

router.put('/blog/:blogId', controller.blog.update)

router.delete('/blog/:blogId', controller.blog.delete)

router.get('/user/blog', controller.user.blogList)

router.get('/user/blog/:blogId', controller.user.blogDetails)


module.exports = router