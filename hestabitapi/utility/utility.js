let bcrypt = require('bcrypt');
let saltRounds = 10;

module.exports.hashPassword = (password, callback) => {
    bcrypt.hash(password, saltRounds, (err, hash) => {
        callback(err, hash);
    });
};

module.exports.checkPassword = (password, hash, callback) => {
    bcrypt.compare(password, hash, (err, hash) => {
        callback(err, hash);
    });
};
