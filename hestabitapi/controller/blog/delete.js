'use strict';

let blogModel = require('../../models/blog');
const mongoose = require('mongoose');

module.exports = (req, res) => {
    // delete a blog
    blogModel.deleteOne(
        { _id: mongoose.Types.ObjectId(req.params.blogId) },
        (err, deleteBlog) => {
            if (err) {
                console.log(err);
                return res.status(400).json({ success: false, isError: true, error: err });
            } else {
                if (deleteBlog > 0) {
                    return res.status(200).json({ success: true, message: "Blog deleted successfully.", blog: deleteBlog });
                }
                else {
                    return res.status(201).json({ success: false, message: "No blog is deleted.", blog: {} });
                }
            }
        });
}