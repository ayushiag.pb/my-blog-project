'use strict';

let blogModel = require('../../models/blog');

module.exports = (req, res) => {
    let conditions = {
        title: req.body.title,
        description: req.body.description,
        category: req.body.category,
        image: req.body.image,
        userId: req.decoded._id,
        created: Date.now(),
        updated: Date.now()
    }
    // create a blog
    blogModel.create(conditions,
        (err, createBlog) => {
            if (err) {
                console.log(err);
                return res.status(400).json({ success: false, isError: true, error: err });
            } else {
                if (createBlog) {
                    return res.status(200).json({ success: true, message: "Blog created successfully.", blog: createBlog });
                }
                else {
                    return res.status(201).json({ success: false, message: "No blog is created.", blog: {} });
                }
            }
        });
}