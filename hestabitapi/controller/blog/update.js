'use strict';

let blogModel = require('../../models/blog');


module.exports = (req, res) => {
    // update a single blog
    blogModel.updateOne(
        { _id: mongoose.Types.ObjectId(req.params.blogId) },
        (err, updateBlog) => {
            if (err) {
                console.log(err);
                return res.status(400).json({ success: false, isError: true, error: err });
            } else {
                if (updateBlog > 0) {
                    return res.status(200).json({ success: true, message: "Blog updated successfully.", blog: updateBlog });
                }
                else {
                    return res.status(201).json({ success: false, message: "No blog is updated.", blog: {} });
                }
            }
        });
}