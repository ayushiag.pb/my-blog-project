'use strict';

let blogModel = require('../../models/blog');

module.exports = (req, res) => {
    // get list of all blogs
    blogModel.find(
        {},
        (err, blogList) => {
            if (err) {
                console.log(err);
                return res.status(400).json({ success: false, isError: true, error: err });
            } else {
                if (blogList.length > 0) {
                    return res.status(200).json({ success: true, message: "List of blog/s added.", blogs: blogList });
                }
                else {
                    return res.status(201).json({ success: false, message: "No blog added yet.", blogs: [] });
                }
            }
        });
}