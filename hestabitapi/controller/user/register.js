'use strict';

let userModel = require('../../models/user');
const utility = require('../../utility/utility');
const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
let config = require('config');


let checkUserExist = (req, res, next) => {
    let conditions = {
        email: req.body.email
    }
    userModel.find(
        conditions,
        (err, user) => {
            if (err) {
                return res.status(400).json({ success: false, isError: true, error: err })
            } else {
                if (user.length > 0) {
                    return res.status(201).json({ success: false, message: 'Email already exists. Register with other email.' })
                } else {
                    next();
                }
            }
        }
    );
}

let generateHashedPassword = (req, res, next) => {
    utility.hashPassword(req.body.password, (err, hashPassword) => {
        if (err) {
            return res.status(400).json({ success: false, isError: true, error: err });
        } else {
            if (hashPassword) {
                req.data = {};
                req.data.hashPassword = hashPassword;
                next();
            } else {
                return res.status(201).json({ success: false, message: 'Something wents wrong in creating hashed password.' });
            }
        }
    })
}


let createUser = (req, res) => {
    // create a user
    userModel.create(
        { email: req.body.email, password: req.data.hashPassword, created: Date.now(), updated: Date.now() },
        (err, createuser) => {
            if (err) {
                console.log(err);
                return res.status(400).json({ success: false, isError: true, error: err });
            } else {
                if (createuser) {
                    //genrate token and send user with token
                    const payload = {
                        email: createuser.email,
                        _id: createuser._id,
                        id: createuser.id
                    };
                    const token = jwt.sign(payload, config.secret, {
                        expiresIn: config.tokenDuration // expires in 24 hours
                    });
                    createUser.token = token;
                    // return the information including token as JSON
                    res.json({
                        success: true,
                        message: 'User registered successfully.',
                        token: token,
                        user: createUser
                    });                }
                else {
                    return res.status(201).json({ success: false, message: "No user is created.", user: {} });
                }
            }
        });
}
module.exports = [
    checkUserExist,
    generateHashedPassword,
    createUser
]