'use strict';

let userModel = require('../../models/user');
const utility = require('../../utility/utility');
const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
let config = require('config');

let login = (req, res, next) => {
    let conditions = {
        email: req.body.email
    }
    userModel.findOne(
        conditions,
        (err, user) => {
            if (err) {
                return res.status(400).json({ success: false, isError: true, error: err })
            } else {
                if (user) {
                    req.data = {};
                    req.data.user = JSON.parse(JSON.stringify(user));
                    next();
                } else {
                    return res.status(201).json({ success: false, message: 'No user exists.' })
                }
            }
        }
    );
}

let comparePassword = (req, res, next) => {
    utility.checkPassword(req.body.password, req.data.user.password, (err, hashPassword) => {
        if (err) {
            return res.status(400).json({ success: false, isError: true, error: err });
        } else {
            if (hashPassword) {
                next();
            } else {
                return res.status(201).json({ success: false, message: 'You have enterd wrong email or password.' });
            }
        }
    })
}


let generateToken = (req, res) => {
    //genrate token and send user with token
    const payload = {
        email: req.data.user.email,
        _id: req.data.user._id,
        id: req.data.user.id
    };
    const token = jwt.sign(payload, config.secret, {
        expiresIn: config.tokenDuration // expires in 24 hours
    });
    delete req.data.user.password;
    req.data.user.token = token;
    // return the information including token as JSON
    res.json({
        success: true,
        message: 'You are logged in successfully.',
        user: req.data.user,
        token
    });
}

module.exports = [
    login,
    comparePassword,
    generateToken
]