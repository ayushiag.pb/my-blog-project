'use strict';

let blogModel = require('../../models/blog');
let mongoose = require('mongoose');

module.exports = (req, res) => {
    // get details of a single blog
    let conditions = {
        _id: mongoose.Types.ObjectId(req.params.blogId),
        userId: mongoose.Types.ObjectId(req.decoded._id)
    }
    blogModel.findOne(
        conditions,
        (err, blogDetails) => {
            if (err) {
                console.log(err);
                return res.status(400).json({ success: false, isError: true, error: err });
            } else {
                if (blogDetails) {
                    return res.status(200).json({ success: true, message: "Details of blog.", blog: blogDetails });
                }
                else {
                    return res.status(201).json({ success: false, message: "No blog added yet.", blog: {} });
                }
            }
        });
}