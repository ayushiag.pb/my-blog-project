'use strict';

const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
const config = require('config'); // get our config file

let authentication = (req, res, next) => {

    // check header or url parameters or post parameters for token
    const token = req.headers['x-access-token'];

    // decode token
    if (token) {

        // verifies secret and checks exp
        jwt.verify(token, config.secret, (err, decoded) => {
            if (err) {
                return res.status(401).send({ success: false, message: 'Authentication Failed.', tokenAuthorization: false });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });

    } else {

        // if there is no token
        // return an error
        return res.status(403).send({
            success: false,
            message: "No token provided."
        });

    }
}

module.exports = authentication;